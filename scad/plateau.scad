$fn=200; 

d_in = 22.5;
e = 4;
h = 60;

l_patte = 12;
e_patte = 10;
e_decoupe_patte = 1;

r_vis = 1.23;
r_passage_vis = 1.6;

r_in = d_in/2;
r_ext = r_in + e;

h_cone = 20;
h_top = 10;

l_plateau = 100;
e_plateau = 5;

module RoundCube(x, y, z, r){
    hull(){
        translate([r, r, 0]) cylinder(r=r, h=z);
        translate([x-r, r, 0]) cylinder(r=r, h=z);
        
        translate([r, y-r, 0]) cylinder(r=r, h=z);
        translate([x-r, y-r, 0]) cylinder(r=r, h=z);
    }
}

module Collier(){
difference(){
    union(){
        translate([0,0,e_plateau/2]) cylinder(r =  r_ext, h = h, center = true);
        
        rotate([0, 0, 45]){
           difference(){ 
             translate([-l_plateau/2,-l_plateau/2,h/2]) RoundCube(l_plateau, l_plateau, e_plateau, 10);
             translate([-(l_plateau-30)/2,-(l_plateau-30)/2,h/2-0.1]) RoundCube(l_plateau-30, l_plateau-30, e_plateau/2, 10);
                
           }
        }
        // patte
        translate([r_in, -e_patte/2, -h/2 + e_plateau/2]) cube([l_patte, e_patte, h]);
        
        intersection(){
            union(){
                rotate([0, 0, 0]) translate([0, 0, 0]) cube([l_plateau*2, 10, h*2], true);
                rotate([0, 0, 90]) translate([0, 0, 0]) cube([l_plateau*2, 10,  h*2], true);
            }
            translate([0,0, e_plateau/2]) cylinder(h+e_plateau, 0, l_plateau*1.2/2, true);
        }

    }
    union(){
        // bois int
        cylinder(r = r_in, h = h+1, center = true);
        
        // decoupe patte
         translate([r_in-0.5, -e_decoupe_patte/2, -h/2]) cube([l_patte*2.5+1, e_decoupe_patte, h+1]);
        
         // passage_vis
        translate([r_in + e/2 + l_patte/2, 0, -20 -r_vis/2]) rotate([90, 0, 0]) cylinder(r = r_vis, h = 20);
        translate([r_in + e/2 + l_patte/2, 20, -20 -r_passage_vis/2]) rotate([90, 0, 0]) cylinder(r = r_passage_vis, h = 20);
        
                 // passage_vis
        translate([r_in + e/2 + l_patte/2, 0, -5 -r_vis/2]) rotate([90, 0, 0]) cylinder(r = r_vis, h = 20);
        translate([r_in + e/2 + l_patte/2, 20, -5 -r_passage_vis/2]) rotate([90, 0, 0]) cylinder(r = r_passage_vis, h = 20);
        
        translate([l_plateau*1.2/2, 0, 0]) cylinder(100, 2,2, true);
        translate([l_plateau*1.2/2, 0, 0]) cylinder(h+2, 4,4, true);  
        
        translate([-l_plateau*1.2/2, 0, 0]) cylinder(100, 2,2, true);
        translate([-l_plateau*1.2/2, 0, 0]) cylinder(h+2, 4,4, true);  
        
        translate([0, l_plateau*1.2/2, 0]) cylinder(100, 2,2, true);
        translate([0, l_plateau*1.2/2, 0]) cylinder(h+2, 4,4, true);  
        
        translate([0, -l_plateau*1.2/2, 0]) cylinder(100, 2,2, true);
        translate([0, -l_plateau*1.2/2, 0]) cylinder(h+2, 4,4, true);  
    }
}
}

module Cone(){
    difference(){
    union(){
       cylinder(r =  r_ext, h = h_cone, center = true);
        translate([0, 0, h_cone/2 + h_top/2] ) cylinder(h = h_top, r1 = r_ext, r2 = 0, center = true);
    }
    union(){
        cylinder(r = 1, h = 50);
        // bois int
        cylinder(r = r_in, h = h_cone+0.1, center = true);
        translate([0, 0, h_cone/2 + (h_top-e)/2] ) cylinder(h = h_top-e, r1 = r_in, r2 = 0, center = true);
        
    //cube([100, 100, 100]);
    }
}
}

Collier();
   