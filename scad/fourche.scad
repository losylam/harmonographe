    $fn=200; 


h_stylo = 10;
r_ext_stylo = 12;
r_int_stylo = 6;
r_vis_stylo = 1.3;

l_tige = 70;
c_ext_tige = 10;
c_int_tige = 4.5;
prof_tige = 20;

h_step_roulement = 1;
r_step_roulement = 5;
dec_roulement = 35;
h_in_roulement = 8;
r_roulement = 4;

h_roulement = 7;
r_ext_roulement = 11;
r_plus_roulement = 3;
h_plus_roulement = 2;

clear_fourche_down = 0.1;

module Roulement(x, y, z){
    translate([x, y, z]){
            difference(){
                cylinder(r = 11, h = 7, center = true);
                cylinder(r = 4, h = 9, center = true);
            };
        }
    }
    
// Roulement(-dec_roulement, 0, c_ext_tige/2 + 3.5 + h_step_roulement);
    
module SupportStylo(){
difference(){
    union(){
        cylinder(h = h_stylo, r = r_ext_stylo, center = true); 
        translate([-l_tige/2, 0, 0]) cube([l_tige, c_ext_tige, c_ext_tige], center = true);
        
        translate([-dec_roulement, 0, h_step_roulement/2]) cylinder(h = c_ext_tige + h_step_roulement, r = r_step_roulement, center = true);
        translate([-dec_roulement, 0, (h_step_roulement+h_in_roulement)/2]) cylinder(h = c_ext_tige + h_in_roulement, r = r_roulement, center = true);
    }
    union(){
        translate([-(l_tige/2 + prof_tige), 0, 0]) cube([l_tige, c_int_tige, c_int_tige], center = true);
        
        cylinder(h = h_stylo + 1, r = r_int_stylo, center = true);
        translate([5, 0, 0]) rotate([0, 90, 0]) cylinder(h = 15, r = r_vis_stylo, center = true);
                    translate([-35, 0, -5]) cylinder(r = 1.6, h = 10, center = true);
    }
}   
}

dec_fourche = 30;
module Fourche(){
    difference(){
        union(){
            cylinder(r = r_ext_roulement + r_plus_roulement, h = 2 * h_plus_roulement + 2*h_roulement + 2 * h_step_roulement + c_ext_tige, center = true);
 
            translate([0, -(r_ext_roulement + r_plus_roulement+dec_fourche)/2, 0]) 
            cube([2*(r_ext_roulement + r_plus_roulement-3), 
                  dec_fourche+r_ext_roulement + r_plus_roulement, 
                  2 * h_plus_roulement + 2*h_roulement + 2 * h_step_roulement + c_ext_tige], center = true);
        }
        union(){
            cylinder(h = 50, r = r_roulement + 4, center = true);
            cube([50, 2*(r_ext_roulement + r_plus_roulement)+5, c_ext_tige+2*h_step_roulement], center = true);
            
            cylinder(h = 2*h_roulement + 2 * h_step_roulement + c_ext_tige + 0.1, r = r_ext_roulement, center = true);
             //
                rotate([0, 90, 0]) translate([-(c_ext_tige/2 + h_step_roulement+h_roulement+h_plus_roulement), -( dec_fourche+r_ext_roulement + r_plus_roulement), 0])  cylinder(r = 10, h = 50, center = true);
                rotate([0, 90, 0]) translate([(c_ext_tige/2 + h_step_roulement+h_roulement+h_plus_roulement), -( dec_fourche+r_ext_roulement + r_plus_roulement), 0])  cylinder(r = 10, h = 50, center = true);
        }
    }
}

module Fourche2(){
    difference(){
        union(){
            cylinder(r = r_ext_roulement + r_plus_roulement, h = h_plus_roulement + h_roulement, center = true);
            translate([0, 0, 0])
            translate([0, -l_tige/2, 0]) cube([c_ext_tige, l_tige, c_ext_tige-1], center = true);
  
        }
        union(){
            translate([0, 0, -h_plus_roulement+0.99])
           cylinder(h=h_roulement, r = r_ext_roulement, center = true);
            cylinder(h = 50, r = r_roulement+4, center = true);
           translate([0, -l_tige/2 - prof_tige, 0]) cube([c_int_tige, l_tige, c_int_tige], center = true);  
            translate([0, -40, -5]) cylinder(r = 1.6, h = 10, center = true);
        }
    }
    
}
r_vis_up = 1.35;

module FourcheUp(){
    difference(){
         Fourche();
        translate([0, 0, -250]) cube([500, 500, 500], center = true);
    }

    translate([0, -25, 0])
        difference(){
            translate([0, -l_tige/2, 0]) cube([c_ext_tige, l_tige, c_ext_tige], center = true);
            translate([0, -l_tige/2 - prof_tige, 0]) cube([c_int_tige, l_tige, c_int_tige], center = true);   
           translate([0, -26, 0]) cylinder(h = 30, r = r_vis_up, center = true); 
    }
}

r_vis_down = 1.6;
r_tete_vis = 3.5;

module FourcheDown(){
    difference(){
        Fourche();
        union(){
            translate([0, 0, 250]) cube([500, 500, 500], center = true);
            translate([0, -22, 0]) translate([0, -l_tige/2, 0]) cube([c_ext_tige+2*clear_fourche_down, l_tige+2*clear_fourche_down, c_ext_tige+2*clear_fourche_down], center = true);
            translate([0, -26, 0]) cylinder(h = 30, r = r_vis_down, center = true);
            translate([0, -26, -13.5]) cylinder(h = 3, r = r_tete_vis, center = true);
        }
    }



}
    SupportStylo();

//translate([-dec_roulement, 0, 0]) color([0, 1.0, 0]) FourcheDown();

//translate([-dec_roulement, 0, c_ext_tige/2 + 1 + (7+2)/2]) color([0, 1.0, 0]) Fourche2();

//translate([-dec_roulement, 0, 0]) color([1.0, 0, 0])  FourcheUp();

