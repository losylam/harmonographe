    $fn=200; 

delta_bearing_in = 19;
delta_bearing_out = 40;
L_bearing = 7;
R_bearing = 11;
R_clear_bearing = 0.1;

//h_in = 28;
//r_in_in = 17;
//r_in_out = 26;

h_in = 28;
r_in_in = 20;
r_in_out = 30;


r_pico_in = 3.93;
l_pico_in = 15;
l_base_pico_in = 7;
delta_l_pico_in = 4;
clear_pico_in = 0.15;
base_pico_in = 6;

h_vis_in = 10;
d_vis_in = 2.7;
d_clear_vis_in = 0.3;
d_tete_vis_in = 3.5;
l_clear_vis_in = 6;

module Bearing(x, y, z){
    translate([x, y, z]){
        rotate([90, 0, 90]){
            difference(){
                cylinder(r = 11, h = 7, center = true);
                cylinder(r = 4, h = 9, center = true);
            };
        }
    }
}

module Gimbal_in(){
    //Bearing(-delta_bearing_in, 0, 0);
    Bearing(delta_bearing_in, 0, 0);
    //
    //rotate([0, 0, 90]){
    //    Bearing(-delta_bearing_out, 0, 0);
    //    Bearing(delta_bearing_out, 0, 0);
    //
    //}
    color([0, 1, 0, 0.5])
    difference(){
        difference(){
            cylinder(r = r_in_out, h = h_in, center = true);
            cylinder(r = r_in_in, h = h_in+2, center = true);
        }
        rotate([90, 0, 90]){
            union(){       
                cylinder(r =  7, h = r_in_out * 3, center = true);
                cylinder(r =  R_bearing + R_clear_bearing, h = (delta_bearing_in + L_bearing / 2)* 2, center = true);
            };
        };
    }
}

module Gimbal_in_1(){
    difference(){
        union(){
            difference(){
                Gimbal_in();
                translate([100, 0, 0]) cube([200, 200, 200], center = true);
            }
            rotate([90, 0, 0]) 
            translate([0, 0, r_in_in + l_base_pico_in/2 + delta_l_pico_in]) 
            cylinder(r = base_pico_in, h = l_base_pico_in, center = true); 
            
            rotate([90, 0, 0]) 
            translate([0, 0, r_in_in + l_pico_in/2 + delta_l_pico_in]) 
            cylinder(r = r_pico_in, h = l_pico_in, center = true); 
            
        }
        union(){
            rotate([-90, 0, 0]) 
            translate([0, 0, r_in_in + l_pico_in/2 + delta_l_pico_in - clear_pico_in]) 
            cylinder(r = base_pico_in + clear_pico_in, h = l_pico_in, center = true); 
            
            //pas de vis
            translate([0, r_in_out - (r_in_out - r_in_in)/2, h_vis_in])
            rotate([0, 90, 0])
            cylinder(r = d_vis_in/2, h = 100, center = true);
            
            translate([0, r_in_out - (r_in_out - r_in_in)/2, -h_vis_in])
            rotate([0, 90, 0])
            cylinder(r = d_vis_in/2, h = 100, center = true);
            
            // passage vis
            translate([0, -(r_in_out - (r_in_out - r_in_in)/2), h_vis_in])
            rotate([0, 90, 0])
            cylinder(r = d_clear_vis_in + d_vis_in/2, h = 100, center = true);
            
            translate([0, -(r_in_out - (r_in_out - r_in_in)/2), -h_vis_in])
            rotate([0, 90, 0])
            cylinder(r = d_clear_vis_in + d_vis_in/2, h = 100, center = true);
            
            // tete de vis
            translate([-(25+l_clear_vis_in), -(r_in_out - (r_in_out - r_in_in)/2), h_vis_in])
            rotate([0, 90, 0])
            cylinder(r = d_tete_vis_in, h = 50, center = true);
            
            translate([-(25+l_clear_vis_in), -(r_in_out - (r_in_out - r_in_in)/2), -h_vis_in])
            rotate([0, 90, 0])
            cylinder(r = d_tete_vis_in, h = 50, center = true);
        }
    }
}
 
module Gimbal_in_2(){
    difference(){
        union(){
            difference(){
                Gimbal_in();
                translate([100, 0, 0]) cube([200, 200, 200], center = true);
            }

            rotate([90, 0, 0]) 
            translate([0, 0, r_in_in + l_base_pico_in/2 + delta_l_pico_in]) 
            cylinder(r = base_pico_in, h = l_base_pico_in, center = true); 
            
                        rotate([-90, 0, 0]) 
            translate([0, 0, r_in_in + l_base_pico_in/2 + delta_l_pico_in]) 
            cylinder(r = base_pico_in, h = l_base_pico_in, center = true); 
            
            
           rotate([90, 0, 0]) 
            translate([0, 0, r_in_in + l_pico_in/2 + delta_l_pico_in]) 
            cylinder(r = r_pico_in, h = l_pico_in, center = true); 
          
           rotate([-90, 0, 0]) 
            translate([0, 0, r_in_in + l_pico_in/2 + delta_l_pico_in]) 
            cylinder(r = r_pico_in, h = l_pico_in, center = true); 
          
            
        }
        union(){
//            rotate([-90, 0, 0]) 
//            translate([0, 0, r_in_in + l_pico_in/2 + delta_l_pico_in - clear_pico_in]) 
//            cylinder(r = base_pico_in + clear_pico_in, h = l_pico_in, center = true); 
//                        rotate([90, 0, 0]) 
//            translate([0, 0, r_in_in + l_pico_in/2 + delta_l_pico_in - clear_pico_in]) 
//            cylinder(r = base_pico_in + clear_pico_in, h = l_pico_in, center = true); 
//            
//                        rotate([-90, 0, 0]) 
//            translate([0, 0, r_in_in + l_pico_in/2 + delta_l_pico_in - clear_pico_in]) 
//            cylinder(r = base_pico_in + clear_pico_in, h = l_pico_in, center = true); 
            
            //pas de vis
            translate([0, r_in_out - (r_in_out - r_in_in)/2, h_vis_in])
            rotate([0, 90, 0])
            cylinder(r = d_vis_in/2, h = 100, center = true);
            
            translate([0, r_in_out - (r_in_out - r_in_in)/2, -h_vis_in])
            rotate([0, 90, 0])
            cylinder(r = d_vis_in/2, h = 100, center = true);
            
            // passage vis
            translate([0, -(r_in_out - (r_in_out - r_in_in)/2), h_vis_in])
            rotate([0, 90, 0])
            cylinder(r = d_clear_vis_in + d_vis_in/2, h = 100, center = true);
            
            translate([0, -(r_in_out - (r_in_out - r_in_in)/2), -h_vis_in])
            rotate([0, 90, 0])
            cylinder(r = d_clear_vis_in + d_vis_in/2, h = 100, center = true);
            
            // tete de vis
            translate([-(25+l_clear_vis_in), -(r_in_out - (r_in_out - r_in_in)/2), h_vis_in])
            rotate([0, 90, 0])
            cylinder(r = d_tete_vis_in, h = 50, center = true);
            
            translate([-(25+l_clear_vis_in), -(r_in_out - (r_in_out - r_in_in)/2), -h_vis_in])
            rotate([0, 90, 0])
            cylinder(r = d_tete_vis_in, h = 50, center = true);
        }
    }
}

delta_semelle = 38;
l_semelle = 8;
L_semelle = 12;
delta_patte_semelle = 12;
r_semelle = 17;

module semelle(){
    difference(){
        union(){
            rotate([90, 0, 0])
            cylinder(h = L_semelle, r = r_semelle, center = true);
            translate([0,0, -10])
            cube([r_semelle*2, L_semelle, 16], center = true);
            translate([0,0, -15])
            cube([(r_semelle+delta_patte_semelle)*2 , L_semelle, 8], center = true);
//            translate([0, -L_semelle/2, -15])
//            difference(){
//                cylinder(r = r_semelle+delta_patte_semelle, h = l_semelle,  center = true);
//                translate([0, 50, 0]) cube([100, 100, 100], center = true);
//        }
    }
        union(){
            translate([0, 3.1, 0])
            rotate([90, 0, 0])
            cylinder(h = 7, r = R_bearing+R_clear_bearing, center = true);
            
            rotate([90, 0, 0])
            cylinder(h=50, r = 7, center = true);
            
            translate([r_semelle+delta_patte_semelle/2, 0, 0])
            cylinder(h = 50, r = 1.4, center = true);
            translate([-(r_semelle+delta_patte_semelle/2), 0, 0])
            cylinder(h = 50, r = 1.4, center = true);
            translate([0, -(r_semelle+delta_patte_semelle/2), 0])
            cylinder(h = 50, r = 1.4, center = true);
            
            
            translate([r_semelle+delta_patte_semelle/2, 0, 0])
            cylinder(h = 27, r = 3.5, center = true);
            translate([-(r_semelle+delta_patte_semelle/2), 0, 0])
            cylinder(h = 27, r = 3.5, center = true);
            translate([0, -(r_semelle+delta_patte_semelle/2), 0])
            cylinder(h = 27, r = 3.5, center = true);


        }
    }
}

module noeud(){
    difference(){
        union(){
            rotate([0, 90, 0])
            cylinder(h = 31, r = 6, center = true);
            rotate([0, 90, 0])
            cylinder(h = 31+16, r = r_pico_in, center = true);
            cylinder(h = 50, r = 24/2, center = true);
        }
        union(){
            cylinder(h = 51, r = 20/2, center = true); 
            translate([0, -50, -50])
            cube([100, 100, 100]);
        } 
    }
}


Gimbal_in_2();
noeud();
translate([0, -delta_semelle, 0])
semelle();