# Harmonographe

![Harmonographe](doc/pdb2019.jpg)

L'harmonographe est constitué de 3 pendules en interaction : 
- 1 pendule avec un cardan (balancement dans deux directions) qui supporte une feuille de papier
- 2 pendules qui balancent dans une direction perpendiculaires sont relié à un stylo par l'intermédiaire de deux tringles

## Matériel nécessaire

Pour la partie mécanique (sans prendre en compte les pieds de la table)
- Du contreplaqué assez épais pour la table
- Trois tiges rondes en bois de 20mm de diamètre ou plus (22.5mm pour la tige centrale qui supporte le plateau)
- 2 tringles en aluminium (cimaise) pour relier le stylo au pendule
- 8 roulements à bille de roller 608zz pour les pendules et 1 pour le support de stylo
- de la fonte de musculation, beaucoup.

## Fichiers 3d

### Cardan

```gimbal.scad``` contient les trois parties constituant le cardan, qui doivent être imprimées en double :
* ```gimbal_in```: partie intermédiaire du cardan
* ```noeud```: partie intérieure du cardan (fixée au pendule du plateau) 
* ```semelle```: partie extérieure du cardan (fixée à la table)
  
Sans le cardan, le noeud et la semelle peuvent être utilisés pour faire une liaison pivot pour les deux autres pendules

### Autres

* ```fourche.scad```: support du stylo
* ```plateau.scad```: plateau support de papier

## Points à améliorer

* La partie "noeud" (qui relie les tiges au roulements) nécessite un système de serrage sur les tiges qui n'est pas présent.
* Les semelles (support de roulements) peuvent facilement être un peu décalée l'une par rapport à l'autre à cause des "imprécisions" de l'impression 3d. 

## Crédits

Réalisé par [Laurent Malys](https://www.laurent-malys.fr) dans le cadre d'une residence artistique accueillie par le service culturel de l'Université du Havre.
